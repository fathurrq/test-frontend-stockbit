const words = ["asd", "sad", "fathur", "good", "doog", "bruh"];

Array.prototype.alphabetSorter = function() {
    var n = this.length;
    while(n !== 0) {
    var newn = 0;
    for(var i = 1; i < this.length; i++) {
    if(this[i] < this[i - 1]) {
    var save = this[i];
    this[i] = this[i - 1];
    this[i - 1] = save;
    newn = i;
    }
    }
    n = newn;
    }
    return this;
   }

function sortCharAlphabetically(str) {
    if (!str) {
      return;
    }
    arrayWord = Array.from(str);;
    sortedWord = arrayWord.alphabetSorter();
    stringWord = sortedWord.reduce((a, b)=> {return b+ ','+a})
    return stringWord;
  }
    
  function arrayAnagrams(words) {
    const anag = {}; 

    words.forEach((word) => {
      const sortedWord = sortCharAlphabetically(word);
      if (anag[sortedWord]) {
        return anag[sortedWord].push(word);
      }
      anag[sortedWord] = [word];
    });
    return anag;
  }
  
  const groupedAnagrams = arrayAnagrams(words);

  for (const sortedWord in groupedAnagrams) {
    console.log(groupedAnagrams[sortedWord]);
  }
  